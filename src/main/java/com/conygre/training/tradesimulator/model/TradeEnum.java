package com.conygre.training.tradesimulator.model;

public enum TradeEnum {
    CREATED("CREATED"),
    PROCESSING("PROCESSING"),
    FILLED("FILLED"),
    REJECTED("REJECTED");

    private String tEnum;

    private TradeEnum(String tEnum) {
        this.tEnum = tEnum;
    }

    public String getState() {
        return this.tEnum;
    } 
}
