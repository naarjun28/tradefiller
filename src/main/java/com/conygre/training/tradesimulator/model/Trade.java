package com.conygre.training.tradesimulator.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Trade {

    @Id
    private String id;
    @JsonFormat(pattern="dd/MM/yyyy")
    private Date date;
    private TradeEnum tEnum = TradeEnum.CREATED;
    private TradeType tradeType = TradeType.BUY;
    private String stockTicker;
    private int stockQuantity;
    private double requestedPrice;


    public Date getCreated() {
        return date;
    }

    public void setCreated(Date date) {
        this.date = date;
    }

    
    public String getstockTicker() {
        return stockTicker;
    }

    public void setstockTicker(String stockTicker) {
        this.stockTicker = stockTicker;
    }

    public double getstockQuantity() {
        return stockQuantity;
    }

    public void setstockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public TradeType getType() {
        return tradeType;
    }

    public void setType(TradeType tradeType) {
        this.tradeType = tradeType;
    }

    public double getPrice() {
        return requestedPrice;
    }

    public void setPrice(double requestedPrice) {
        this.requestedPrice = requestedPrice;
    }

    public TradeEnum gettEnum() {
        return tEnum;
    }

    public void settEnum(TradeEnum tEnum) {
        this.tEnum = tEnum;
    }
}
